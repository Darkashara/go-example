package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type ItemNameIn struct {
	Id int `json:"id"`
	Name string `json:"name"`
}

type Item struct {
	Items []ItemNameIn `json:"items"`
}


func main(){
	var data Item
	plan, errRead := ioutil.ReadFile("./items-names.json")
	if errRead != nil{
		fmt.Println(errRead)
		os.Exit(0)
	}
	err := json.Unmarshal([]byte(plan), &data)
	if err != nil{
		fmt.Println(err)
		os.Exit(0)
	}
	fmt.Println(data)
}

func (n *ItemNameIn) UnmarshalJSON(buf []byte) error {
	tmp := []interface{}{&n.Id, &n.Name}
	wantLen := len(tmp)
	if err := json.Unmarshal(buf, &tmp); err != nil {
		return err
	}
	if g, e := len(tmp), wantLen; g != e {
		return fmt.Errorf("wrong number of fields in ItemNameIn: %d != %d", g, e)
	}
	return nil
}
